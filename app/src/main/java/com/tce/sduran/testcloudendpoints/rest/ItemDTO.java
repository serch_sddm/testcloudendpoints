package com.tce.sduran.testcloudendpoints.rest;

import java.io.Serializable;

/**
 * Created by sduran on 01/10/2015.
 */
public class ItemDTO implements Serializable {

    private String text;
    private String done;
    private String id;
    private String created;
    private String kind;

    public ItemDTO() {

    }

    public ItemDTO(String text, String done, String id, String created, String kind) {
        this.text = text;
        this.done = done;
        this.id = id;
        this.created = created;
        this.kind = kind;
    }

    public String getText() {
        return text;
    }

    public String getDone() {
        return done;
    }

    public String getId() {
        return id;
    }

    public String getCreated() {
        return created;
    }

    public String getKind() {
        return kind;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setDone(String done) {
        this.done = done;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public void setCreated(String created) {
        this.created = created;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ItemDTO{");
        sb.append("text='").append(text).append('\'');
        sb.append(", done='").append(done).append('\'');
        sb.append(", id='").append(id).append('\'');
        sb.append(", created='").append(created).append('\'');
        sb.append(", kind='").append(kind).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
