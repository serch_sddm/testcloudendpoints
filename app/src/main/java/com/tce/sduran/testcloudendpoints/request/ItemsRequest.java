package com.tce.sduran.testcloudendpoints.request;

import android.content.res.Resources;

import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;
import com.tce.sduran.testcloudendpoints.R;
import com.tce.sduran.testcloudendpoints.rest.ItemsRequestDTO;
import com.tce.sduran.testcloudendpoints.rest.ItemsResponseDTO;


public class ItemsRequest extends SpringAndroidSpiceRequest<ItemsResponseDTO> {

    private ItemsRequestDTO Itemsrequest;
    private Resources resources;

    public ItemsRequest(ItemsRequestDTO request, Resources resources) {
        super(ItemsResponseDTO.class);
        this.Itemsrequest = request;
        this.resources = resources;
    }

    @Override
    public ItemsResponseDTO loadDataFromNetwork() throws Exception {
        String url = resources.getString(R.string.endpoint1) + resources.getString(R.string.resource_items) ;
        ItemsResponseDTO response = getRestTemplate().postForObject(url, Itemsrequest, ItemsResponseDTO.class);
        return response;
    }
}
