package com.tce.sduran.testcloudendpoints.rest;

import java.io.Serializable;


public class ItemsResponseDTO implements Serializable {

    private String created;

    public String getCreated() { return created;  }

    public void setCreated(String created) { this.created = created;  }

    private String done;

    public String getDone() {
        return done;
    }

    public void setDone(String done) {
        this.done = done;
    }

    private String etag;

    public String getEtag() {
        return etag;
    }

    public void setEtag(String etag) {
        this.etag = etag;
    }

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String kind;

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ItemsResponse{");
        sb.append("created='").append(created).append('\'');
        sb.append(", done='").append(done).append('\'');
        sb.append(", etag='").append(etag).append('\'');
        sb.append(", id='").append(id).append('\'');
        sb.append(", kind='").append(kind).append('\'');
        sb.append(", text='").append(text).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
