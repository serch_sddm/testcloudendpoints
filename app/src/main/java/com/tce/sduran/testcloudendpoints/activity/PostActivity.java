package com.tce.sduran.testcloudendpoints.activity;


import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.tce.sduran.testcloudendpoints.R;
import com.tce.sduran.testcloudendpoints.request.ItemsRequest;
import com.tce.sduran.testcloudendpoints.rest.ItemsRequestDTO;
import com.tce.sduran.testcloudendpoints.rest.ItemsResponseDTO;

import java.util.Random;

public class PostActivity extends AppCompatBaseActivity {

    EditText mEtText;
    TextView mTvResult;
    ProgressBar mPb;
    String text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);

        mEtText = (EditText) findViewById(R.id.et_text);
        mTvResult = (TextView) findViewById(R.id.tv_post_result);
        mPb = (ProgressBar) findViewById(R.id.pb_post);

    }

    public void add(View view){
        mTvResult.setText("");
        text = mEtText.getText().toString();
        executeProgress();
    }

    private void executeProgress() {

        mPb.setVisibility(View.VISIBLE);
        mTvResult.setVisibility(View.GONE);
        attemptRESTItems();
    }

    private void attemptRESTItems() {

        ItemsRequest request = new ItemsRequest(new ItemsRequestDTO(text), getResources());
        getSpiceManager().execute(request, new ItemsRequestListener());

    }

    private final class ItemsRequestListener implements RequestListener<ItemsResponseDTO> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {

            mPb.setVisibility(View.GONE);
            mTvResult.setVisibility(View.VISIBLE);

            //displays a feedback message on the screen
            mTvResult.setText("Error: " + spiceException.getMessage());

        }

        @Override
        public void onRequestSuccess(ItemsResponseDTO result) {

            mPb.setVisibility(View.GONE);
            mTvResult.setVisibility(View.VISIBLE);
            mTvResult.setText( "text:"+result.getText() + "\n\ndone:"+result.getDone()+ "\n\nid:"+result.getId()+ "\n\ncreated:"+result.getCreated()+ "\n\nkind:"+result.getKind() );
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
