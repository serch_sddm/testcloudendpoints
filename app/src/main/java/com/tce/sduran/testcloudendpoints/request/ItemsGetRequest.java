package com.tce.sduran.testcloudendpoints.request;

import android.content.res.Resources;

import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;
import com.tce.sduran.testcloudendpoints.R;
import com.tce.sduran.testcloudendpoints.rest.ItemsGetResponseDTO;
import com.tce.sduran.testcloudendpoints.rest.ItemsRequestDTO;
import com.tce.sduran.testcloudendpoints.rest.ItemsResponseDTO;


public class ItemsGetRequest extends SpringAndroidSpiceRequest<ItemsGetResponseDTO> {

    private Resources resources;

    public ItemsGetRequest(Resources resources) {
        super(ItemsGetResponseDTO.class);
        this.resources = resources;
    }

    @Override
    public ItemsGetResponseDTO loadDataFromNetwork() throws Exception {
        String url = resources.getString(R.string.endpoint1) + resources.getString(R.string.resource_items) ;
        ItemsGetResponseDTO response = getRestTemplate().getForObject(url, ItemsGetResponseDTO.class);
        return response;
    }
}
