package com.tce.sduran.testcloudendpoints.rest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class ItemsGetResponseDTO implements Serializable {

    private String nextPageToken;
    private List<ItemDTO> items;
    private String kind;
    private String etag;


    public String getNextPageToken() {
        return nextPageToken;
    }

    public void setNextPageToken(String nextPageToken) {
        this.nextPageToken = nextPageToken;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getEtag() {
        return etag;
    }

    public void setEtag(String etag) {
        this.etag = etag;
    }

    public List<ItemDTO> getItems() {
        return items;
    }

    public void setItems(List<ItemDTO> items) {
        List<ItemDTO> mItems = new ArrayList<ItemDTO>();
        for(ItemDTO item : items){
            mItems.add(new ItemDTO(item.getText(),item.getDone(), item.getId(), item.getCreated(), item.getKind()));
        }
        this.items = mItems;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ItemsGetResponseDTO{");
        sb.append("nextPageToken='").append(nextPageToken).append('\'');
        sb.append(", items=").append(items);
        sb.append(", kind='").append(kind).append('\'');
        sb.append(", etag='").append(etag).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
