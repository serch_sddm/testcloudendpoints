package com.tce.sduran.testcloudendpoints.rest;

import java.io.Serializable;

public class ItemsRequestDTO implements Serializable {

    private String text;

    public ItemsRequestDTO(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }



    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ItemsRequest{");
        sb.append("text='").append(text).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
