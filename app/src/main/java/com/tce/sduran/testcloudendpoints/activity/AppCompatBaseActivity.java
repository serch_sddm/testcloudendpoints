package com.tce.sduran.testcloudendpoints.activity;

import android.support.v7.app.AppCompatActivity;

import com.octo.android.robospice.JacksonSpringAndroidSpiceService;
import com.octo.android.robospice.SpiceManager;


public class AppCompatBaseActivity extends AppCompatActivity {

    @Override
    protected void onStart() {
        spiceManager.start(this);
        super.onStart();
    }

    @Override
    protected void onStop() {
        spiceManager.shouldStop();
        super.onStop();
    }


    protected SpiceManager getSpiceManager() {
        return spiceManager;
    }


    private SpiceManager spiceManager = new SpiceManager(JacksonSpringAndroidSpiceService.class);


}
