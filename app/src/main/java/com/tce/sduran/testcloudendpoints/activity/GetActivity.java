package com.tce.sduran.testcloudendpoints.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.tce.sduran.testcloudendpoints.R;
import com.tce.sduran.testcloudendpoints.request.ItemsGetRequest;
import com.tce.sduran.testcloudendpoints.request.ItemsRequest;
import com.tce.sduran.testcloudendpoints.rest.ItemDTO;
import com.tce.sduran.testcloudendpoints.rest.ItemsGetResponseDTO;
import com.tce.sduran.testcloudendpoints.rest.ItemsRequestDTO;
import com.tce.sduran.testcloudendpoints.rest.ItemsResponseDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GetActivity extends AppCompatBaseActivity {

    TextView mTvResult;
    ProgressBar mPb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get);

        mTvResult = (TextView) findViewById(R.id.tv_get_result);
        mPb = (ProgressBar) findViewById(R.id.pb_get);
        executeProgress();
    }

    private void executeProgress() {
        mPb.setVisibility(View.VISIBLE);
        mTvResult.setVisibility(View.GONE);
        attemptRESTItems();
    }

    private void attemptRESTItems() {

        ItemsGetRequest request = new ItemsGetRequest(getResources());
        getSpiceManager().execute(request, new ItemsGetRequestListener());

    }

    private final class ItemsGetRequestListener implements RequestListener<ItemsGetResponseDTO> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {

            mPb.setVisibility(View.GONE);
            mTvResult.setVisibility(View.VISIBLE);
            //displays a feedback message on the screen
            mTvResult.setText("Error: " + spiceException.getMessage());

        }

        @Override
        public void onRequestSuccess(ItemsGetResponseDTO result) {

            mPb.setVisibility(View.GONE);
            mTvResult.setVisibility(View.VISIBLE);

            final StringBuilder sbItems = new StringBuilder('\n');

            for ( ItemDTO item : result.getItems()){
                sbItems.append("\n");
                sbItems.append(" text='").append(item.getText()).append("\n");
                sbItems.append(" done='").append(item.getDone()).append("\n");
                sbItems.append(" id='").append(item.getId()).append("\n");
                sbItems.append(" created='").append(item.getCreated()).append("\n");
                sbItems.append(" kind='").append(item.getKind()).append("\n");
            }

            mTvResult.setText("nextPageToken: "+result.getNextPageToken() + "\n\nitems: "+sbItems.toString() + "\n\nkind: "+result.getKind()+ "\n\netag: "+result.getEtag() );
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_get, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
